import cv2
import numpy as np
import sys
import time

scale = 1
# cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture(sys.argv[1])
car_cascade = cv2.CascadeClassifier('cars.xml')

#blank image
width  = int(cap.get(3))
height = int(cap.get(4))
blank_image = np.zeros((int(height/scale)/2,int(width/scale),3), np.uint8)
blank_image = cv2.resize(blank_image, (width/scale,height/scale))
blank_image = blank_image[int(height/scale)/2:, :int(width/scale)]
fgbg = cv2.createBackgroundSubtractorMOG2()

frame_counter = 0
point_counter = 0

# OPTICAL FLOW
# *****************************************
# def draw_flow(img, flow, step=16):
    # h, w = img.shape[:2]
    # y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    # fx, fy = flow[y,x].T
    # lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    # lines = np.int32(lines + 0.5)
    # vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # cv2.polylines(vis, lines, 0, (0, 255, 0))
    # # for (x1, y1), (_x2, _y2) in lines:
        # # cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    # return vis

# # read first frame for optical flow
# ret, frame = cap.read()
# if not ret:
    # print "***ERROR*** Can't read frame"
    # sys.exit(0)
# resized = cv2.resize(frame, (width/scale,height/scale))
# roi = resized[int(height/scale)/2:, :int(width/scale)]
# roi = cv2.blur(roi, (5,5))
# prevgray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)

fps = 0
frame_counter = 0
while True:
    if frame_counter == 0:
        time_start = time.time()
    frame_counter +=1    #measure fps
    ret, frame = cap.read()
    if not ret:
        print "***ERROR*** Can't read frame"
        break
    
    resized = cv2.resize(frame, (width/scale,height/scale))
    roi = resized[int(height/scale)/2:, :int(width/scale)]
    roi_blur = cv2.blur(roi, (5,5))
    
    #remove background
    fgmask = fgbg.apply(roi_blur)
    
    #remove noise and car
    #*****************************************
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))  # kernel to apply to the morphology
    closing = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    dilation = cv2.dilate(closing, kernel)
    ret, thresh = cv2.threshold(fgmask, 200, 255, cv2.THRESH_BINARY_INV)
    thresh_blur = cv2.blur(thresh, (20,20))
    ret, thresh = cv2.threshold(thresh_blur, 200, 255, cv2.THRESH_BINARY)
    #remove car
    road = cv2.bitwise_and(roi_blur,roi_blur,mask=thresh)
    cv2.imshow('road',road)
    
    # hough line
    #*****************************************
    road_gray = cv2.cvtColor(road, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(road_gray, 75, 150)
    try:
        # lines = cv2.HoughLinesP(image=edges,rho=1,theta=np.pi/540, threshold=80,lines=np.array([]), minLineLength=200,maxLineGap=180)
        lines = cv2.HoughLinesP(image=edges,rho=1,theta=np.pi/540, threshold=80,lines=np.array([]), minLineLength=200,maxLineGap=180)
        a,b,c = lines.shape
        for i in range(a):
            cv2.line(roi, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), (0, 255, 0), 1, cv2.LINE_AA)
            # cv2.line(blank_image, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), (0, 255, 255), 1, cv2.LINE_AA)
    except:
        print "No have line!"
        
    cv2.imshow('roi',roi)
    # cv2.imshow('blank_image',blank_image)
    
    # # find optical flow
    # flow = cv2.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    # prevgray = gray
    # cv2.imshow('flow', draw_flow(gray, flow))
    
    #get time elapsed until 1 second, compute FPS
    time_elapsed = time.time() - time_start
    if time_elapsed >= 1.0:
        fps = frame_counter
        frame_counter=0
        print "FPS: "+str(fps)
        # blank_image = np.zeros((int(height/scale)/2,int(width/scale),3), np.uint8)

    k = cv2.waitKey(1)
    if k == 27:
        break
    elif k == 13:
        cv2.imwrite("lane_preprocess.png", blank_image)
        import draw_lane

cap.release()
cv2.destroyAllWindows()
